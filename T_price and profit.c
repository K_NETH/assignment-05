#include <stdio.h>
int NoOfAttendees(int T_price);
int Revenue(int T_price);
int Cost(int T_price);
int Profit(int T_price);

int NoOfAttendees(int T_price){
    return 120-(T_price-15)/5*20;
}

int Revenue(int T_price){
    return NoOfAttendees(T_price)*T_price;
}

int Cost(int T_price){
    return NoOfAttendees(T_price)*3+500;
}

int Profit(int T_price){
    return Revenue(T_price)-Cost(T_price);
}

int main(){
    int T_price;
    printf("Expected profit for the ticket prices\n\n");
    for(T_price=5;T_price<50;T_price+=5){
        printf("Ticket Price = Rs.%d \t Profit = Rs.%d \n",T_price,Profit(T_price));
    }
    return 0;
}
